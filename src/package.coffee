`#!/usr/bin/env node
`
###
package.json usage:
  "scripts": {
    "preversion": "[[ -z $(git status --porcelain) ]] && pack-sym --git && git add package.json && git commit -m 'Added symlinks'"
  }
###

fs = require "fs"
path = require "path"
exec = require("child_process").exec
argv = require('minimist') process.argv.slice 2

argv.ignore ?= []
argv.ignore = [argv.ignore] if typeof argv.ignore is "string"

argv.ignore.push "./node_modules/" if argv.node_modules

find = (whitelist, blacklist) ->
  exec "find -L ./ -xtype l -print0 | xargs -0 ls -plah", {cwd: process.cwd()}, (err, stdout, stderr) ->
    pkg = require "#{process.cwd()}/package.json"
    pkg.symlinks = {} if pkg.symlinks
    if err
      console.log "Error: #{err}"
      process.exit 1
    for l in stdout.split "\n"
      continue if l.replace(/\s/, '').length < 1
      try
        reg = /^l([rwx-]{9})\s*([0-9]+)\s*([a-zA-Z0-9]+)\s*([a-zA-Z0-9]+)\s*([0-9]+)\s*([A-Za-z]{3}\s*[123]?[0-9]\s*[012]?[0-9]:[0-5][0-9])\s*(.*) ->\s*(.*)$/
        [l, perms, numLinks, user, group, size, modTime, symlink, dest] = reg.exec l
        #continue for entry in whitelist when l.indexOf(entry) > 0 if whitelist
        continue if whitelist and symlink not in whitelist
        stop = no
        stop = yes for entry in blacklist when symlink.indexOf(entry) >= 0 if blacklist
        continue if blacklist and stop
        pkg.symlinks ?= {}
        #console.log "Symlinking to #{dest} from #{symlink} (#{path.join path.dirname(symlink), dest})"
        #pkg.symlinks[symlink] = path.join "./", path.dirname(symlink), dest
        pkg.symlinks[symlink] = dest
      catch error
        console.log "Unable to run regex on #{l}: #{error}"
    #console.log JSON.stringify pkg, null, 2
    fs.writeFileSync "#{process.cwd()}/package.json", JSON.stringify pkg, null, 2


findGit = (cb) ->
  exec "git ls-files", {cwd: process.cwd()}, (err, stdout, stderr) ->
    if err
      console.log "Error: #{err}"
      process.exit 1
    cb stdout.split("\n").map((s) -> "./#{s}")

if argv.git
  findGit (gitrefs) ->
    find gitrefs
else
  find null, argv.ignore
