`#!/usr/bin/env node
`
###
package.json usage:
  "scripts": {
    "install": "unpack-sym"
  }
###

fs = require "fs"
path = require "path"
mkdirp = require "mkdirp"

for source, dest of require("#{process.cwd()}/package.json").symlinks
  do (source, dest) ->
    mkdirp path.dirname(source), (err) ->
      return console.log "Creating #{path.dirname(source)} to symlink #{source} -> #{dest}: #{err}" if err
      fs.symlink dest, source, (err) ->
        if err
          # Report error unless symlink already existed to correct target.
          fs.readlink source, (read_err, target) ->
            if read_err or target isnt dest
              console.log "Error symlinking #{source} -> #{dest}: #{err}"
            else
              console.log "Info: Symlink #{source} -> #{dest} already exists."
        