# Test symlink packaging

## Make
npm run make

## Create directory
rm -rf has-symlinks
mkdir has-symlinks
cd has-symlinks
npm init -y
npm install --save ../../
## Symlinks
### has-symlinks/test -> has-symlinks/node_modules/npm-pkg-sym/package.json
ln -s node_modules/npm-pkg-sym/package.json test
### has-symlinks/src/b -> has-symlinks/src/a
mkdir -p src/a/
ln -s a src/b
## Run `npm-pkg-sym`
node_modules/.bin/pack-sym --node_modules
node -e "p = require('./package.json'); if(Object.keys(p.symlinks).length !== 2 || p.symlinks['./src/b'] !== 'a' || p.symlinks['./test'] !== 'node_modules/npm-pkg-sym/package.json'){ process.exit(1) }"
if [ $? -eq 1 ]
then
  echo "Symlinks inside has-symlinks/package.json are incorrect."
  exit 1
fi
cd ../
rm -rf has-symlinks

# Test packaging with --git

rm -rf git-symlinks
mkdir git-symlinks
cd git-symlinks
git init
npm init -y
npm install --save ../../
## Symlinks
### git-symlinks/test -> git-symlinks/node_modules/npm-pkg-sym/package.json
ln -s node_modules/npm-pkg-sym/package.json test
### git-symlinks/src/b -> git-symlinks/src/a
mkdir -p src/a/
cd src/
ln -s a b
cd ../
git add src/b
## Run `npm-pkg-sym`
node_modules/.bin/pack-sym --git
node -e "p = require('./package.json'); if(Object.keys(p.symlinks).length !== 1 || p.symlinks['./src/b'] !== 'a'){ process.exit(1) }"
if [ $? -eq 1 ]
then
  echo "Symlinks inside git-symlinks/package.json are incorrect."
  exit 1
fi
cd ../
rm -rf git-symlinks

# Test unpackaging

rm -rf needs-symlinks
mkdir needs-symlinks
cd needs-symlinks
npm init -y
npm install --save ../../
mkdir -p src/a/
(cd src && ln -s a to_a && ln -s b to_b)
node -e "p = require('./package.json'); p.symlinks = {'./src/b': 'a', './src/to_a': 'a', './src/to_b': 'a'}; require('fs').writeFileSync('./package.json', JSON.stringify(p, null, 2));"
out=$(node_modules/.bin/unpack-sym)
echo "------Output------"
echo "$out"
echo "------------------"
if [ -L src/b ] ; then
  if [ -e src/b ] ; then
    echo "src/b created successfully"
  else
    echo "src/b exists, but is a broken link"
    exit 1
  fi
else
  echo "src/b doesn't exist"
  exit 1
fi
# to_a -> already existed.
if !(echo "$out" | grep Info | grep -q 'to_a -> a'); then
  echo "No info message linking pre-existing to_a -> a."
fi
if !(echo "$out" | grep Error | grep -q 'to_b -> a'); then
  echo "No error message linking to_b -> a with pre-existing to_b -> b."
fi


cd ../
rm -rf needs-symlinks
