#!/usr/bin/env node
;

/*
package.json usage:
  "scripts": {
    "preversion": "[[ -z $(git status --porcelain) ]] && pack-sym --git && git add package.json && git commit -m 'Added symlinks'"
  }
 */
var argv, exec, find, findGit, fs, path,
  indexOf = [].indexOf || function(item) { for (var i = 0, l = this.length; i < l; i++) { if (i in this && this[i] === item) return i; } return -1; };

fs = require("fs");

path = require("path");

exec = require("child_process").exec;

argv = require('minimist')(process.argv.slice(2));

if (argv.ignore == null) {
  argv.ignore = [];
}

if (typeof argv.ignore === "string") {
  argv.ignore = [argv.ignore];
}

if (argv.node_modules) {
  argv.ignore.push("./node_modules/");
}

find = function(whitelist, blacklist) {
  return exec("find -L ./ -xtype l -print0 | xargs -0 ls -plah", {
    cwd: process.cwd()
  }, function(err, stdout, stderr) {
    var dest, entry, error, group, i, j, l, len, len1, modTime, numLinks, perms, pkg, ref, ref1, reg, size, stop, symlink, user;
    pkg = require((process.cwd()) + "/package.json");
    if (pkg.symlinks) {
      pkg.symlinks = {};
    }
    if (err) {
      console.log("Error: " + err);
      process.exit(1);
    }
    ref = stdout.split("\n");
    for (i = 0, len = ref.length; i < len; i++) {
      l = ref[i];
      if (l.replace(/\s/, '').length < 1) {
        continue;
      }
      try {
        reg = /^l([rwx-]{9})\s*([0-9]+)\s*([a-zA-Z0-9]+)\s*([a-zA-Z0-9]+)\s*([0-9]+)\s*([A-Za-z]{3}\s*[123]?[0-9]\s*[012]?[0-9]:[0-5][0-9])\s*(.*) ->\s*(.*)$/;
        ref1 = reg.exec(l), l = ref1[0], perms = ref1[1], numLinks = ref1[2], user = ref1[3], group = ref1[4], size = ref1[5], modTime = ref1[6], symlink = ref1[7], dest = ref1[8];
        if (whitelist && indexOf.call(whitelist, symlink) < 0) {
          continue;
        }
        stop = false;
        if (blacklist) {
          for (j = 0, len1 = blacklist.length; j < len1; j++) {
            entry = blacklist[j];
            if (symlink.indexOf(entry) >= 0) {
              stop = true;
            }
          }
        }
        if (blacklist && stop) {
          continue;
        }
        if (pkg.symlinks == null) {
          pkg.symlinks = {};
        }
        pkg.symlinks[symlink] = dest;
      } catch (error1) {
        error = error1;
        console.log("Unable to run regex on " + l + ": " + error);
      }
    }
    return fs.writeFileSync((process.cwd()) + "/package.json", JSON.stringify(pkg, null, 2));
  });
};

findGit = function(cb) {
  return exec("git ls-files", {
    cwd: process.cwd()
  }, function(err, stdout, stderr) {
    if (err) {
      console.log("Error: " + err);
      process.exit(1);
    }
    return cb(stdout.split("\n").map(function(s) {
      return "./" + s;
    }));
  });
};

if (argv.git) {
  findGit(function(gitrefs) {
    return find(gitrefs);
  });
} else {
  find(null, argv.ignore);
}
