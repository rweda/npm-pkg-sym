#!/usr/bin/env node
;

/*
package.json usage:
  "scripts": {
    "install": "unpack-sym"
  }
 */
var dest, fn, fs, mkdirp, path, ref, source;

fs = require("fs");

path = require("path");

mkdirp = require("mkdirp");

ref = require((process.cwd()) + "/package.json").symlinks;
fn = function(source, dest) {
  return mkdirp(path.dirname(source), function(err) {
    if (err) {
      return console.log("Creating " + (path.dirname(source)) + " to symlink " + source + " -> " + dest + ": " + err);
    }
    return fs.symlink(dest, source, function(err) {
      if (err) {
        return fs.readlink(source, function(read_err, target) {
          if (read_err || target !== dest) {
            return console.log("Error symlinking " + source + " -> " + dest + ": " + err);
          } else {
            return console.log("Info: Symlink " + source + " -> " + dest + " already exists.");
          }
        });
      }
    });
  });
};
for (source in ref) {
  dest = ref[source];
  fn(source, dest);
}
