Packages symbolic links into package.json to be unpacked on npm install.

## Installation

Install `npm-pkg-sym` as a standard dependency (as it needs to run on `npm install` to unpack links).

    npm install git+ssh://git@gitlab.com/rweda/npm-pkg-sym.git#v1.1.0

To pack/unpack automatically on `npm version` and `npm install`, insert the following into package.json:

    {
      "scripts": {
        "preversion": "pack-sym --git && git add package.json && git commit -m 'Added symlinks'",
        "install": "unpack-sym"
      }
    }


## Development

`npm test` prior to commit.
This does `npm run make` and runs tests.
Visually inspect output.
